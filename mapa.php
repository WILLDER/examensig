<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>trabajo final</title>
    <style>
      html, body {
        background-color: rgb(168, 244, 247);
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        width: 100%;
        height: 80%;
      }
      #coords{width: 200px;}
    </style>
  </head>
  <body >
    <h1 align="center">PROYECTO FINAL</h1>
    <div id="map"></div>
    <form method="POST" action="guardar_map.php" autocomplete="">
    <b>Nombre</b>
    <input type="text" require style="width: 200px;"id="" placeholder="" name="nombre"/>
    <b>Latitud</b>
    <input type="text" require id="coords" placeholder="latitud" name="latitud"/>
    <b>Longitud</b>
    <input type="text" require id="coord" placeholder="longitud" name="longitud"/>
    <input type="submit" value="Guardar">
    <button value="ver registro"><a href="formulario.php">ver registro</a></button>
    </form>
    <script>


var marker;          
var coords = {};    

initMap = function () 
{
        navigator.geolocation.getCurrentPosition(
          function (position){
            coords =  {
              lng: position.coords.longitude,
              lat: position.coords.latitude
            };
            setMapa(coords);
            
           
          },function(error){console.log(error);});  
}

function setMapa (coords)
{   
      var map = new google.maps.Map(document.getElementById('map'),
      {
        zoom: 13,
        center:new google.maps.LatLng(coords.lat,coords.lng),
      });

      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        position: new google.maps.LatLng(coords.lat,coords.lng),
      });
      
      marker.addListener( 'dragend', function (event)
      {
        document.getElementById("coords").value = this.getPosition().lat();
        document.getElementById("coord").value = this.getPosition().lng();
      });
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
</body>
</html>